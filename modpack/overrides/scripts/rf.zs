import minetweaker.item.IItemStack as Stack;
import minetweaker.item.IIngredient as Ingred;

val Piston = <ore:craftingPiston>;
val CObsidian = <Railcraft:cube:4>;
val PGlass = <ore:paneGlass>;

val HobbySteamEngine = <Railcraft:machine.beta:7>;
val CommercialSteamEngine = <Railcraft:machine.beta:8>;
val IndustrialSteamEngine = <Railcraft:machine.beta:9>;

val BronzePlate = <ore:plateBronze>;
val IronPlate = <ore:plateIron>;
val SteelPlate = <ore:plateSteel>;

val BronzeGear = <ore:gearBronze>;
val IronGear = <ore:gearIron>;
val SteelGear = <ore:gearSteel>;

val IngotLead = <ore:ingotLead>;
val IngotInvar = <ore:ingotInvar>;
al IngotIron = <ore:ingotIron>;
val IngotGold = <ore:ingotGold>;

val BasicCircuitB = <Forestry:chipsets>;
val EnhancedCircuit = <Forestry:chipsets:1>;
val RefinedCircuit = <Forestry:chipsets:2>;
val IntricateCircuit = <Forestry:chipsets:3>;

val Chipset = <ore:chipsetRed>;
val ChipsetIron = <ore:chipsetIron>;
val ChipsetGold = <ore:chipsetGold>;
val ChipsetDiamond = <ore:chipsetDiamond>;
val ChipsetEmerald = <ore:chipsetEmerald>;

//Engine Nerfs
recipes.remove(HobbySteamEngine);
recipes.remove(CommercialSteamEngine);
recipes.remove(IndustrialSteamEngine);
addEngineRecipe(BronzePlate, BasicCircuitB, BronzeGear, HobbySteamEngine);
addEngineRecipe(IronPlate, RefinedCircuit, IronGear, CommercialSteamEngine);
addEngineRecipe(SteelPlate, IntricateCircuit, SteelGear, IndustrialSteamEngine);

//Functions
function addEngineRecipe(p as Ingred, c as Ingred, g as Ingred, o as Stack){
	recipes.addShaped(o, [[p, p, p], [null, c], [g, Piston, g]]);
}
